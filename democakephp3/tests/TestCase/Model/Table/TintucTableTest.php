<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TintucTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TintucTable Test Case
 */
class TintucTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TintucTable
     */
    public $Tintuc;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tintuc'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tintuc') ? [] : ['className' => 'App\Model\Table\TintucTable'];
        $this->Tintuc = TableRegistry::get('Tintuc', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tintuc);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
