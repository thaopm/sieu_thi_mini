<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Utility\Text;
use Cake\Utility\Inflector;

use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\Auth\DigestAuthenticate;


class CategoriesController extends AppController
{   
     
    public function index()
    {
        $categories = TableRegistry::get('Categories');
        
        $category = $categories->find('all');
        $this->set('category',$category);
        
    }

   
    public function view($id = null)
    {   
        $categories = TableRegistry::get('Categories');
        $category = $categories->get($id);

        $category->slug = strtolower(Text::slug($category->name,'-'));
            // pr($category);die;
        $this->set('category', $category);
        $this->set('_serialize', ['category']);
    }

    public function add()
    {
        $categories = TableRegistry::get('Categories');
        $category = $categories->newEntity();
        if ($this->request->is('post')) {
            // $category = $this->request->getData();
          
            $category = $categories->patchEntity($category, $this->request->getData()); 
            $category->slug = strtolower(Text::slug($category->name,'-'));

            
            $save = $categories->save($category);
            if ($save) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $this->set(compact('category'));
        $this->set('_serialize', ['category']);
    }

    public function edit($id = null)
    {
        $categories = TableRegistry::get('Categories');
        $category = $categories->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $categories->patchEntity($category, $this->request->getData());
            $category->slug = strtolower(Text::slug($category->name,'-'));
            $this->set('save' , $this->Categories->save($category));
// pr($category);echo"<hr>";            
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('save success.'));
// pr($category);die;
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('save fail.'));
        }
        $this->set(compact('category'));
        $this->set('_serialize', ['category']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get($id);
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
