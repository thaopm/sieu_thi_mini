<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tintuc Controller
 *
 * @property \App\Model\Table\TintucTable $Tintuc
 *
 * @method \App\Model\Entity\Tintuc[] paginate($object = null, array $settings = [])
 */
class TintucController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $tintuc = $this->paginate($this->Tintuc);

        $this->set(compact('tintuc'));
        $this->set('_serialize', ['tintuc']);
       
        // $theloai = $this->Tintuc->find()
		
		// 	->select(['id', 'Ten'])
		//     ->where(['id >=' => 4])
		// 	->contain(['Loaitin'=>['id','Ten']])
		//     ->orderDesc('created_at')
		//     ->toArray();
		// $this->set('theloai',$theloai);
    }

    /**
     * View method
     *
     * @param string|null $id Tintuc id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tintuc = $this->Tintuc->get($id, [
            'contain' => []
        ]);

        $this->set('tintuc', $tintuc);
        $this->set('_serialize', ['tintuc']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tintuc = $this->Tintuc->newEntity();
        if ($this->request->is('post')) {
            $tintuc = $this->Tintuc->patchEntity($tintuc, $this->request->getData());
            if ($this->Tintuc->save($tintuc)) {
                $this->Flash->success(__('The tintuc has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tintuc could not be saved. Please, try again.'));
        }
        $this->set(compact('tintuc'));
        $this->set('_serialize', ['tintuc']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tintuc id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tintuc = $this->Tintuc->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tintuc = $this->Tintuc->patchEntity($tintuc, $this->request->getData());
            if ($this->Tintuc->save($tintuc)) {
                $this->Flash->success(__('The tintuc has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tintuc could not be saved. Please, try again.'));
        }
        $this->set(compact('tintuc'));
        $this->set('_serialize', ['tintuc']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tintuc id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tintuc = $this->Tintuc->get($id);
        if ($this->Tintuc->delete($tintuc)) {
            $this->Flash->success(__('The tintuc has been deleted.'));
        } else {
            $this->Flash->error(__('The tintuc could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
