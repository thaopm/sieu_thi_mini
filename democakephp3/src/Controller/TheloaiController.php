<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;


class TheloaiController extends AppController
{	
	public function index(){
		$theloai = TableRegistry::get('Theloai');
		//truy vấn liên kết giữa 2 bảng theloai va loaitin
        
		$tl = $theloai->find('all',[
			'conditions' => ['Theloai.id >' => 4],
			'fields'=>['id', 'Ten'],
			'contain'=>[],
			'limit'=>2
		]);
		$this->set('theloai',$tl);

	}
}
?>